#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>

void* worker(void *arg){
	long id;
	id = (long) arg;
	printf("Hello World! I am thread #%ld\n", id);
	pthread_exit(0);
}
void main(int argc, char **argv){
	int n = atoi(argv[1]);

	if(argc < 2 || n < 1 || n >10){
		printf("Error: number out of bounds [1-10]");
		return;
	}else{
		pthread_t tids[n];
		for(long unsigned int i = 0; i<n; i++){
			tids[i] = i+1;
			pthread_attr_t attr;
			pthread_attr_init(&attr);
			printf("Creating thread #%ld\n", tids[i]);
			pthread_create(&tids[i], &attr, worker, (void *)tids[i]);
		}
		for(int i = 0; i<n;i++){
			pthread_join(tids[i], NULL);
		}
	}
}
